﻿using System.Collections.Generic;

namespace WazeBotDiscord.Classes.Roles
{
    public static class Moderator
    {
        public static IReadOnlyDictionary<ulong, ulong> Ids = new Dictionary<ulong, ulong>
        {
            [347386780074377217] = 422840010983866368 // Global/Map Raid
        };
    }
}
