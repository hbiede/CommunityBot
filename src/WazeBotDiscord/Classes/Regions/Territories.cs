﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WazeBotDiscord.Classes.Servers
{
    class Territories
    {
        public static class TerritoriesChannels
        {

        }

        public static class TerritoriesRoles
        {
            public const ulong CountryManager = 352273880758484993;
            public const ulong StateManager = 352276169401565185;
            public const ulong AreaManager = 352276470951051274;
            public const ulong Mentor = 352276852422868992;
            public const ulong Level6 = 352277194195730432;
            public const ulong Level5 = 352277464573018112;
            public const ulong Level4 = 352277522513264640;
            public const ulong Level3 = 352277556789116929;
            public const ulong Level2 = 352277589986902016;
            public const ulong Level1 = 352277704348925952;
            public const ulong ATR = 352277825144881152;
            public const ulong AmericanSamoa = 352278018061893632;
            public const ulong Guam = 352278133396865036;
            public const ulong NorthernMarianaIslands = 352278238543872000;
            public const ulong PuertoRico = 352278265655721985;
            public const ulong VirginIslands = 352278371700310016;
        }
    }
}
